/*
Navicat MySQL Data Transfer

Source Server         : localhost
Source Server Version : 50547
Source Host           : localhost:3306
Source Database       : oceania

Target Server Type    : MYSQL
Target Server Version : 50547
File Encoding         : 65001

Date: 2018-03-14 17:56:42
*/

SET FOREIGN_KEY_CHECKS=0;
-- ----------------------------
-- Table structure for `erp_admin`
-- ----------------------------
DROP TABLE IF EXISTS `erp_admin`;
CREATE TABLE `erp_admin` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'erp用户id',
  `username` varchar(100) DEFAULT NULL COMMENT '用户名',
  `password` varchar(100) DEFAULT NULL COMMENT 'erp密码',
  `create_time` timestamp NULL DEFAULT NULL COMMENT '创建时间',
  `update_time` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
  `online_time` timestamp NULL DEFAULT NULL COMMENT '上线时间',
  PRIMARY KEY (`id`),
  KEY `idx_username` (`username`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of erp_admin
-- ----------------------------
INSERT INTO erp_admin VALUES ('2', 'admin', 'dc18982b536a46aeed8408450c5dd44d', '2018-03-14 13:59:56', '2018-03-14 14:37:02', '2018-03-14 14:37:02');
