<?php if (!defined('THINK_PATH')) exit();?><!DOCTYPE html>
<html lang="zh-cn">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
<meta name="renderer" content="webkit">
<title></title>
<link rel="stylesheet" href="/Public/Erp/Index/css/pintuer.css">
<link rel="stylesheet" href="/Public/Erp/Index/css/admin.css">
<script src="/Public/Erp/Index/js/jquery.js"></script>
<script src="/Public/Erp/Index/js/pintuer.js"></script>
<link rel="stylesheet" href="/Public/bootstrop/bootstrap.min.css">
<script src="https://code.jquery.com/jquery-3.3.1.min.js"></script>
<script src="/Public/bootstrop/bootstrap.min.js"></script>
<script src="/vendor/ueditor/ueditor.config.js"></script>
<script src="/vendor/ueditor/ueditor.all.min.js"></script>
<script src="/vendor/ueditor/lang/zh-cn/zh-cn.js"></script>
</head>
<body>
<div class="panel admin-panel">
  <div class="panel-head"><strong><span class="icon-key"></span> 更新子类NEWS详情</strong></div>
  <div class="body-content">
    <form class="form-x" action="">

      <div class="form-group">
        <div class="label">
          <label for="sitename">标题：</label>
        </div>
        <div class="field">
          <input type="text" id='title' class="input"  value="<?php echo ($info["title"]); ?>" placeholder=""/>
          <input type="hidden" id='subnewsid' value="<?php echo ($info["id"]); ?>"/>
        </div>
      </div>

      <div class="form-group">
        <div class="label">
          <label for="sitename">锚点名称：</label>
        </div>
        <div class="field">
          <input type="text" id='anchor' class="input" value="<?php echo ($info["anchor"]); ?>"  placeholder="name"/>
        </div>
      </div>

      <div class="form-group">
        <div class="label">
          <label for="sitename">排序字段：</label>
        </div>
        <div class="field">
          <input type="text" id='order_status' class="input"  value="<?php echo ($info["order_status"]); ?>"/>
        </div>
      </div>


      <div class="form-group">
        <div class="label">
          <label for="sitename">内容：</label>
        </div>
        <div class="field">
          <textarea id='content'><?php echo ($info["content"]); ?></textarea>
        </div>
      </div>

      <div class="form-group">
        <div class="label">
          <label></label>
        </div>
        <div class="field">
          <button class="button bg-main icon-check-square-o" type="button" onclick="homePageSeo()"> 更新子类NEWS详情</button>
        </div>
      </div>
    </form>
  </div>
</div>
</body></html>
<script type="text/javascript">
          Array.prototype.removeByIndex = function (index) {
              if (index > -1) {
                  this.splice(index, 1);
              }
          }
          var ue = UE.getEditor('content',{
              toolbars:[[ 'link',
                  'unlink', //取消链接
                  '|',
                  'forecolor', //字体颜色
                  'backcolor', //背景色
                  'fontfamily', //字体
                  'fontsize', //字号
                  '|',
                  'bold', //加粗
                  'italic', //斜体
                  'underline', //下划线
                  'strikethrough', //删除线
                  '|',
                  'formatmatch', //格式刷
                  'removeformat', //清除格式
                  '|',
                  'insertorderedlist', //有序列表
                  'insertunorderedlist', //无序列表
                  '|',
                  'inserttable', //插入表格
                  'paragraph', //段落格式
                  //                  'simpleupload', //单图上传
                  'imagecenter', //居中
                  //                  'attachment', //附件
                  '|',
                  'justifyleft', //居左对齐
                  'justifycenter', //居中对齐
                  'horizontal', //分隔线
                  '|',
                  'blockquote', //引用
                  'insertcode', //代码语言
                  '|',
                  'source', //源代码
                  'preview', //预览
                  //                  'fullscreen', //全屏
              ]],
              //focus时自动清空初始化时的内容
              autoClearinitialContent:false,
              //关闭字数统计
              wordCount:false,
              //关闭elementPath
              elementPathEnabled:false,
              //默认的编辑区域高度
              initialFrameHeight:500
              //更多其他参数，请参考ueditor.config.js中的配置项
          })

          function homePageSeo(){
              var subnewsid = $("#subnewsid").val()
              var title   = $('#title').val()
              var content = UE.getEditor('content').getContent()
              var anchor = $('#anchor').val()
              var order_status = $("#order_status").val()
              if($.trim(subnewsid) == ''){
                  alert('系统异常，请联系管理员');return;
              }
              if($.trim(title) == ''){
                  alert('标题不能为空');return;
              }
              if($.trim(content) == ''){
                  alert('内容不能为空');return;
              }
              if($.trim(anchor) == ''){
                  alert('锚点名称不能为空');return;
              }
              if($.trim(order_status) == ''){
                  alert('排序字段不能为空');return;
              }
              $.post("/index.php/Erp/New/saveUpdateSubNews",{subnewsid:subnewsid,title:title,content:content,anchor:anchor,order_status:order_status},function(v){
                  if(v.f){
                      alert(v.data);
                      location.href="/index.php/Erp/New/subNews/id/<?php echo ($info["news_id"]); ?>";
                  }else{
                      alert(v.data);
                      location.reload();
                  }
              },'json')
          }
</script>