<?php if (!defined('THINK_PATH')) exit();?><!DOCTYPE html>
<html lang="zh-cn">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
<meta name="renderer" content="webkit">
<title></title>
<link rel="stylesheet" href="/Public/Erp/Index/css/pintuer.css">
<link rel="stylesheet" href="/Public/Erp/Index/css/admin.css">
<script src="/Public/Erp/Index/js/jquery.js"></script>
<script src="/Public/Erp/Index/js/pintuer.js"></script>
<link rel="stylesheet" href="/Public/bootstrop/bootstrap.min.css">
<script src="https://code.jquery.com/jquery-3.3.1.min.js"></script>
<script src="/Public/bootstrop/bootstrap.min.js"></script>
</head>
<body>
<div class="panel admin-panel">
  <div class="panel-head"><strong><span class="icon-key"></span> 网站首页 SEO & JS</strong></div>
  <div class="body-content">
    <form class="form-x" action="">
      <div class="form-group">
        <div class="label">
          <label for="sitename">站点名称：</label>
        </div>
        <div class="field">
          <input type="text" id='website' class="input w50"  size="50" placeholder="" readonly='readonly' value='<?php echo ($info); ?>'/>
        </div>
      </div>

      <div class="form-group">
        <div class="label">
          <label for="sitename">Meta title：</label>
        </div>
        <div class="field">
          <input type="text" id='title' class="input w50"  size="50" placeholder="" data-validate="required:请输入title" />
        </div>
      </div>

      <div class="form-group">
        <div class="label">
          <label for="sitename">Meta description：</label>
        </div>
        <div class="field">
          <input type="text" id='description' class="input w50"  size="50" placeholder="" data-validate="required:请输入description" />
        </div>
      </div>



      <div class="form-group">
        <div class="label">
          <label for="sitename">统计脚本1：</label>
        </div>
        <div class="field">
         <textarea class="input w50" placeholder="说明：非必填，请输入谷歌等统计信息脚本" id='js_one'></textarea>
        </div>
      </div>

      <div class="form-group">
        <div class="label">
          <label for="sitename">统计脚本2：</label>
        </div>
        <div class="field">
         <textarea class="input w50" placeholder="说明：非必填，请输入谷歌等统计信息脚本" id='js_two'></textarea>
        </div>
      </div>

      <div class="form-group">
        <div class="label">
          <label for="sitename">统计脚本3：</label>
        </div>
        <div class="field">
         <textarea class="input w50" placeholder="说明：非必填，请输入谷歌等统计信息脚本" id='js_three'></textarea>
        </div>
      </div>



      <div class="form-group">
        <div class="label">
          <label for="sitename">Meta keywords：</label>
        </div>
        <div class="field">
           <input type="text" id='keywords' class="input w50"  size="50"/><button type="button" class="btn btn-primary btn-xs" onclick="add_reply()">添加</button>
        </div>
                  <div class="label">
          <label for="sitename"></label>
        </div>
        <div class="field">
            <div id='replay_list' class="col-sm-9"></div>
        </div>
      </div>



      <div class="form-group">
        <div class="label">
          <label></label>
        </div>
        <div class="field">
          <button class="button bg-main icon-check-square-o" type="button" onclick="homePageSeo()"> 添加SEO</button>
        </div>
      </div>      
    </form>
  </div>
</div>
</body></html>
<script type="text/javascript">
          Array.prototype.removeByIndex = function (index) {
              if (index > -1) {
                  this.splice(index, 1);
              }
          }

          function add_reply() {
            var replay = $('#keywords').val();
            if(!$.trim(replay)){
                alert('keywords不能为空');
                return;
            }
            replay_list.push({replay:replay})
            $('#keywords').val('')
            show_replay_list()
        }

       var replay_list = []
        function show_replay_list() {
            $('#remove').remove()
            show_replay = '<div id="remove" class="modelList">'
            for (var i = 0; i < replay_list.length; i++) {
                show_replay += '<div  style="float:left">'
                show_replay +=       '<span class="btn btn-info btn-xs" style="margin: 2px">'
                show_replay +=              replay_list[i].replay
                show_replay +=         '</span>'
                show_replay +=     '<a href="javascript:void(0)">'
                show_replay +=          '<span style="color: red;" onclick=remove_replay_id('+i+')>'
                show_replay +=             '[-]'
                show_replay +=          '</span>'
                show_replay +=       '</a>'
                show_replay += '</div>'
            }
            show_replay+= '</div>'
            $('#replay_list').append(show_replay)
        }
          function remove_replay_id(id) {
              replay_list.removeByIndex(id)
              show_replay_list()
          }

          function homePageSeo(){
              var website = $('#website').val()
              var title   = $('#title').val()
              var description = $('#description').val()
              var keywords = replay_list
              var js_one = $("#js_one").val()
              var js_two = $('#js_two').val()
              var js_three = $('#js_three').val()
              if($.trim(website) == ''){
                  alert('站点名称不能为空');
                  return;
              }
              if($.trim(title) == ''){
                  alert('请填写title');
                  return;
              }
              if($.trim(description) == ''){
                  alert('请填写description');
                  return;
              }
              $.post("/index.php/Erp/Homepage/addHomePageSeo",{website:website,title:title,description:description,keywords:keywords,js_one:js_one,js_two:js_two,js_three:js_three},function(v){
                  if(v.f){
                      alert(v.data);
                      location.href="/index.php/Erp/Web/info";
                  }else{
                      alert(v.data);
                      location.reload();
                  }
              },'json')
          }
</script>