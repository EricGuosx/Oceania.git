<?php if (!defined('THINK_PATH')) exit();?><!DOCTYPE html>
<html lang="zh-cn">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
<meta name="renderer" content="webkit">
<title></title>
<link rel="stylesheet" href="/Public/Erp/Index/css/pintuer.css">
<link rel="stylesheet" href="/Public/Erp/Index/css/admin.css">
<script src="/Public/Erp/Index/js/jquery.js"></script>
<script src="/Public/Erp/Index/js/pintuer.js"></script>
</head>
<body>
<div class="panel admin-panel">
  <div class="panel-head"><strong><span class="icon-key"></span> 修改密码</strong></div>
  <div class="body-content">
    <form class="form-x" action="">
            <div class="form-group">
        <div class="label">
          <label for="sitename">管理员帐号：</label>
        </div>
        <div class="field">
          <label style="line-height:33px;">
           <?php echo ($username); ?>
          </label>
        </div>
      </div>
      <div class="form-group">
        <div class="label">
          <label for="sitename">原始密码：</label>
        </div>
        <div class="field">
          <input type="password" id='oldpassword' class="input w50"  size="50" placeholder="请输入密码" data-validate="required:请输入密码" />
        </div>
      </div>
      <div class="form-group">
        <div class="label">
          <label for="sitename">新密码：</label>
        </div>
        <div class="field">
          <input type="password" id='password' class="input w50" name="newpass" size="50" placeholder="请输入新密码" data-validate="required:请输入密码,length#>=5:密码不能小于5位" />
        </div>
      </div>
      <div class="form-group">
        <div class="label">
          <label for="sitename">确认密码：</label>
        </div>
        <div class="field">
          <input type="password" id='password2' class="input w50" name="renewpass" size="50" placeholder="请再次输入新密码" data-validate="required:请再次输入新密码,repeat#newpass:两次输入的密码不一致" />
        </div>
      </div>
      
      <div class="form-group">
        <div class="label">
          <label></label>
        </div>
        <div class="field">
          <button class="button bg-main icon-check-square-o" type="button" onclick="addNewUser()"> 提交</button>
        </div>
      </div>      
    </form>
  </div>
</div>
</body></html>
<script>
    function addNewUser(){
        var oldpasswprd = $('#oldpassword').val()
        var password = $('#password').val()
        var password2 = $('#password2').val()
        if($.trim(oldpasswprd) == ''){
            alert('请填写原始密码');
        }
        if($.trim(password) =='' || $.trim(password2)==''){
            alert('密码不能为空');
        }
        if($.trim(password2) != $.trim(password)){
            alert('两次密码必须保持一致');
        }
        $.post("/index.php/Erp/User/saveUpdateData",{oldpasswprd:oldpasswprd,password:password,password2:password2},function(v){
            if(v.f){
                alert(v.data);
                location.href="/index.php/Erp/User/userList";
            }else{
                alert(v.data);
                location.reload();
            }
        },'json')
    }
</script>