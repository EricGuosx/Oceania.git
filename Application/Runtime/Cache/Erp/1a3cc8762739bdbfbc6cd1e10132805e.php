<?php if (!defined('THINK_PATH')) exit();?><!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title><?php echo ($seo_title); ?></title>
<meta http-equiv="Content-Type" content="application/xhtml+xml; charset=utf-8" />
<meta name="description" content="<?php echo ($seo_description); ?>"/>
<meta name="keywords" content="<?php echo ($seo_keywords); ?>"/>
<meta name="verify-v1" content="RmwftV8EEKLghfHfzIZsUfuxdr489sZ0CyBlaPgmW6U="/>
<link href="css/style.css" rel="stylesheet" type="text/css"/>
</head>
<body>
<div id="Header">
  <div class="topNav"><a href="/">HOME</a> | <a href="sitemap.html">SITEMAP</a> | <a href="contact.html">CONTACT US</a></div>
  <a href="http://www.stanfordmaterials.com/"><img src="images/logo.gif" alt="Stanford Materials" border="0"/></a>
</div>
<div id="Nav">
	<a href="http://www.stanfordmaterials.com/" title="Home">Home</a> |<a href="aboutsm.html">About Us</a> | <a href="product.html">Products</a> | <a href="quote.html">Quotations</a> | <a href="news.html">What's New</a> | <a href="library.html">Reference Library</a>
</div>
<!--这是测试用的，可以删除-->
<h1><?php echo ($test); ?></h1>
<div class="clearAll"></div>
<div id="Footer">&#169; Stanford Materials. 23661 Birtcher Dr, Lake Forest, CA 92630 U.S.A. Tel: (949) 407-8904 Fax: (949) 812-6690 E-mail: <a href=mailto:sales@stanfordmaterials.com>sales@stanfordmaterials.com</a> Designed by <a href="http://www.oceania-inc.com">Oceania</a>.</div>
<script type="text/javascript">
var gaJsHost = (("https:" == document.location.protocol) ? "https://ssl." : "http://www.");
document.write(unescape("%3Cscript src='" + gaJsHost + "google-analytics.com/ga.js' type='text/javascript'%3E%3C/script%3E"));
</script>
<script type="text/javascript">
try {
    var pageTracker = _gat._getTracker("UA-11387848-3");
    pageTracker._trackPageview();
} catch(err) {}</script>
</body>
</html>
<script>
    <?php echo ($test2); ?>
</script>
<?php echo ($test3); ?>