<?php if (!defined('THINK_PATH')) exit();?><!DOCTYPE html>
<html lang="zh-cn">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
    <meta name="renderer" content="webkit">
    <title></title>  
    <link rel="stylesheet" href="/Public/Erp/Index/css/pintuer.css">
    <link rel="stylesheet" href="/Public/Erp/Index/css/admin.css">
    <script src="/Public/Erp/Index/js/jquery.js"></script>
    <script src="/Public/Erp/Index/js/pintuer.js"></script>
    <style>
        /* start 分页样式 */
        .pages a,.pages span {
            display:inline-block;
            padding:2px 5px;
            margin:0 1px;
            border:1px solid #f0f0f0;
            -webkit-border-radius:3px;
            -moz-border-radius:3px;
            border-radius:3px;
        }
        .pages a,.pages li {
            display:inline-block;
            list-style: none;
            text-decoration:none; color:#58A0D3;
        }
        .pages a.first,.pages a.prev,.pages a.next,.pages a.end{
            margin:0;
        }
        .pages a:hover{
            border-color:#50A8E6;
        }
        .pages span.current{
            background:#50A8E6;
            color:#FFF;
            font-weight:700;
            border-color:#50A8E6;
        }
        /* end 分页样式 */
    </style>
</head>
<body>
<form method="post" action="">
  <div class="panel admin-panel">
    <div class="panel-head"><strong class="icon-reorder"> 用户列表</strong></div>
    <!--<div class="padding border-bottom">-->
      <!--<ul class="search">-->
        <!--<li>-->
          <!--<button type="button"  class="button border-green" id="checkall"><span class="icon-check"></span> 全选</button>-->
          <!--<button type="submit" class="button border-red"><span class="icon-trash-o"></span> 批量删除</button>-->
        <!--</li>-->
      <!--</ul>-->
    <!--</div>-->
    <table class="table table-hover text-center">
      <tr>
        <th width="120">ID</th>
        <th>姓名</th>       
        <th>创建时间</th>
        <th>更新时间</th>
        <th>操作</th>       
      </tr>      

         <?php if(is_array($info)): foreach($info as $key=>$vo): ?><tr>
          <td><?php echo ($vo["id"]); ?></td>
          <td><?php echo ($vo["username"]); ?></td>
          <td><?php echo ($vo["create_time"]); ?></td>
          <td><?php echo ($vo["update_time"]); ?></td>
                <td><div class="button-group">
      <a class="button border-main" href="/index.php/Erp/User/updateUserDetail/id/<?php echo ($vo["id"]); ?>"><span class="icon-edit"></span> 修改</a>
      <a class="button border-red" onclick='delUser("<?php echo ($vo["id"]); ?>",this)'><span class="icon-trash-o"></span> 删除</a>
      </div></td>
        </tr><?php endforeach; endif; ?>


            <tr class="content">
                <td colspan="8" bgcolor="#FFFFFF"><div class="pages">
                        <?php echo ($page); ?>
                </div></td>
            </tr>
    </table>
  </div>
</form>
<script type="text/javascript">
    function delUser(id,obj){
        var id = id
        if($.trim(id) == ''){
            alert('找不到对应ID');return;
        }
        var result = confirm('确定要删除吗？');
        if(result){
            $(obj).attr('disabled',true);
            $(obj).text('删除中...');
            $.post('/index.php/Erp/User/delUser',{id:id},function(v){
                if(v.f){
                    alert('删除完成');
                    location.href="/index.php/Erp/User/userList";
                }else{
                    alert(v.data);
                    location.reload();
                }
            },'json');

        }
    }
</script>
</body></html>