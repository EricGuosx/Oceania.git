<?php
/**
 * function.php
 * User: Eric
 * Date: 2018/3/16
 * Time: 9:09
 * Project: OceaniaErp
 */

/**
 * TODO 获取当前IP
 * @return string
 */
function get_ip()
{
    if (getenv("HTTP_CLIENT_IP") && strcasecmp(getenv("HTTP_CLIENT_IP"), "unknown"))
        $ip = getenv("HTTP_CLIENT_IP");
    else if (getenv("HTTP_X_FORWARDED_FOR") && strcasecmp(getenv("HTTP_X_FORWARDED_FOR"), "unknown"))
        $ip = getenv("HTTP_X_FORWARDED_FOR");
    else if (getenv("REMOTE_ADDR") && strcasecmp(getenv("REMOTE_ADDR"), "unknown"))
        $ip = getenv("REMOTE_ADDR");
    else if (isset($_SERVER['REMOTE_ADDR']) && $_SERVER['REMOTE_ADDR'] && strcasecmp($_SERVER['REMOTE_ADDR'], "unknown"))
        $ip = $_SERVER['REMOTE_ADDR'];
    else
        $ip = "unknown";
    return($ip);
}

/**
 * TODO 返回json数据
 *
 * @param bool $bool
 * @param string $content
 * @return array
 */
function returnAjaxJson($bool=false,$content=''){
    $return = [
        'f' => $bool,
        'data' => $content,
    ];
    echo  json_encode($return);
    exit;
}

/**
 * TODO 基础分页的相同代码封装，使前台的代码更少
 * @param $count 要分页的总记录数
 * @param int $pagesize 每页查询条数
 * @return \Think\Page
 */
function getpage($count, $pagesize = 10) {
    $p = new Think\Page($count, $pagesize);
    $p->setConfig('header', '<li class="rows">共<b>%TOTAL_ROW%</b>条记录&nbsp;第<b>%NOW_PAGE%</b>页/共<b>%TOTAL_PAGE%</b>页</li>');
    $p->setConfig('prev', '上一页');
    $p->setConfig('next', '下一页');
    $p->setConfig('last', '末页');
    $p->setConfig('first', '首页');
    $p->setConfig('theme', '%FIRST%%UP_PAGE%%LINK_PAGE%%DOWN_PAGE%%END%%HEADER%');
    $p->lastSuffix = false;//最后一页不显示为总页数
    return $p;
}

/**
 * TODO 网站唯一标识
 *
 * @param string $value1
 * @param string $value2
 * @param string $suffix
 * @return string
 */
function erp_access_token($value1='',$value2='',$suffix='Oceania')
{
    $time = time();
    $token1 = sha1($value1);
    $token2 = md5($value2);
    $access_token = sha1($time . $token1 . $token2 . $suffix);
    return $access_token;
}

/**
 * TODO 返回登陆用户信息
 *
 * @return array
 */
function erpUserInfo()
{
    $erpUserInfo = [
        'uid'         => $_SESSION['userid'],
        'username'    => $_SESSION['username'],
        'online_time' => $_SESSION['online_time'],
        'website'     => $_SESSION['website'],
    ];
    return $erpUserInfo;
}

/**
 * TODO 检测是否已经有应用的网站
 *
 * @return bool
 */
function checkWebExist()
{
    $webSite = $_SESSION['website'];
    if($webSite){
        return true;
    }
    $checkCount = M('erp_website')->where('status=2')->count();
    if(intval($checkCount) >= 1){
        return true;
    }else{
        return false;
    }
}

/**
 * TODO 获取顶级域名
 *
 * @param $url
 * @return string
 */
function getTopDomain($url)
{
    preg_match('/[\w][\w-]*\.(?:com\.cn|com|cn|co|net|org|gov|cc|biz|info)(\/|$)/isU', $url, $domain);
    $return = rtrim($domain[0], '/');
    return $return;
}

/**
 * TODO 二维数组转一维
 *
 * @param array $arr
 * @return array
 */
function twoToOne($arr=[])
{
    foreach($arr as $k=>$v){
        $new[] = $v['replay'];
    }
    return $new;
}

/**
 * TODO 一维数组转二维
 *
 * @param $str
 * @return array
 */
function oneToTwo($str)
{
    $arr = explode(',',$str);
    foreach($arr as $k=>$v){
        $new[]['replay'] = $v;
    }
    return $new;
}
