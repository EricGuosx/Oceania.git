<?php
/**
 * WebController.class.php
 * User: Eric
 * Date: 2018/3/14
 * Time: 18:25
 * Project: OceaniaErp
 */
namespace Erp\Controller;
use Think\Controller;
class WebController extends ErpController
{
    public function __construct ()
    {
        parent::__construct();
    }

    /**
     * 首页展示网站信息
     */
    public function info()
    {
        $webinfo = $_SESSION['website'];
        if(!$webinfo){
            $result = M('erp_website')->where('status=2')->find();
            $webinfo = $result['website'];
            $_SESSION['website'] = $result['website'];
        }
        $web = $webinfo;
        $info = [
            'ip'           => get_ip(),
            'username'     => $_SESSION['username'],
            'online_time'  => $_SESSION['online_time'],
            'count'        => M('erp_login_log')->count(),
            'website'      => $web,
        ];
        $this->assign('info',$info);
        $this->display();
    }



    /**
     * 站点列表
     */
    public function webList()
    {
        $m = M('erp_website');
        $count = $m->count();
        $p = getpage($count,10);
        $list = M('erp_website')->field(true)->order('id DESC')->limit($p->firstRow, $p->listRows)->select();
        $this->assign('info', $list);
        $this->assign('page', $p->show());
        $this->display();
    }

    /**
     * 添加站点
     */
    public function addWeb()
    {
        //dump($_SESSION);
        $this->display();
    }


    /**
     * ajax添加站点
     */
    public function addNewWeb()
    {
        $data = I('post.');
        if(empty($data['website'])){
            echo returnAjaxJson(false,'请填写网站名称');
        }
        if(empty($data['nickname'])){
            echo returnAjaxJson(false,'请填写网站别名');
        }
        $userinfo =  erpUserInfo();
        $userItem = [
            'website' => $data['website'],
            'nickname' => $data['nickname'],
            'access_token' =>erp_access_token($data['website'],$data['nickname']),
            'create_time' => date('Y-m-d H:i:s',time()),
            'create_user' => $userinfo['username'],
            'update_user' => $userinfo['username'],
        ];
        $id = M('erp_website')->add($userItem);
        if($id){
            echo returnAjaxJson(true,'添加成功');
        }else{
            echo returnAjaxJson(false,'发生意料之外的错误');
        }
    }



    /**
     * 展示修改站点详情
     */
    public function updateWebDetail()
    {
        $id = I('get.id');
        if(!$id){
            echo '这是一个美丽的错误，请联系管理员';
            die;
        }
        $info = M('erp_website')->find($id);
        //dump($info);
        $this->assign('info',$info);
        $this->display();
    }

    /**
     * 更新站点信息
     */
    public function saveUpdateWebData()
    {
        $data = I('post.');
        $id = $data['webId'];
        if(empty($id)){
            returnAjaxJson(false,'发生了意料之外的错误，请联系管理员！！！');
        }
        if(empty($data['nickname'])){
            returnAjaxJson(false,'请填写站点别名');
        }
        if(empty($data['website'])){
            returnAjaxJson(false,'请填写站点名称');
        }
        $userinfo =  erpUserInfo();
        $userItem = [
            'website'      => $data['website'],
            'nickname'     => $data['nickname'],
            'access_token' =>erp_access_token($data['website'],$data['nickname']),
            'update_user'  => $userinfo['username'],
            'update_time'  => date('Y-m-d H:i:s',time()),
        ];
        $result = M('erp_website')->where("id = $id")->save($userItem);
        if($result){
            returnAjaxJson(true,'站点更新成功');
        }else{
            returnAjaxJson(false,'发生意料之外的错误');
        }
    }


    /**
     * 删除站点
     */
    public function delWeb()
    {
        $id = I('post.id');
        if(!$id){
            returnAjaxJson(false,'发生了意料之外的错误，请联系管理员');
        }
        $result = M('erp_website')->where("id = $id")->delete();
        if($result){
            returnAjaxJson(true,'删除完成。');
        }else{
            returnAjaxJson(false,'发生意料之外的错误');
        }
    }

    /**
     * 应用站点
     */
    public function applay()
    {
        $id = I('post.id');
        $check = checkWebExist();
        if($check){
            returnAjaxJson(false,'当前已经有正在处理的网站');
        }
        $userinfo =  erpUserInfo();
        if(!$id){
            returnAjaxJson(false,'发生了意料之外的错误，请联系管理员');
        }
        $item = [
            'update_user'  => $userinfo['username'],
            'update_time'  => date('Y-m-d H:i:s',time()),
            'status'       => 2,
        ];
        $result = M('erp_website')->where("id = $id")->save($item);
        if($result){
            $web = M('erp_website')->where("id = $id")->find();
            $_SESSION['website'] = $web['website'];
            returnAjaxJson(true,'应用成功');
        }else{
            returnAjaxJson(false,'发生意料之外的错误');
        }
    }

    /**
     * 取消应用
     */
    public function cancelApplay()
    {
        $id = I('post.id');
        $userinfo =  erpUserInfo();
        if(!$id){
            returnAjaxJson(false,'发生了意料之外的错误，请联系管理员');
        }
        $item = [
            'update_user'  => $userinfo['username'],
            'update_time'  => date('Y-m-d H:i:s',time()),
            'status'       => 1,
        ];
        $result = M('erp_website')->where("id = $id")->save($item);
        if($result){
            $_SESSION['website'] = '';
            returnAjaxJson(true,'取消成功');
        }else{
            returnAjaxJson(false,'发生意料之外的错误');
        }
    }




}