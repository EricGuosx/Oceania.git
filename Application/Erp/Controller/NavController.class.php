<?php
/**
 * NavController.class.php
 * User: Eric
 * Date: 2018/4/2
 * Time: 15:25
 * Project: OceaniaErp
 */
namespace Erp\Controller;
use Think\Controller;
use Common\Helper\Category;
class NavController extends ErpController
{
    public function __construct ()
    {
        parent::__construct();
    }


    /**
     * 导航列表
     */
    public function navList()
    {
        $category_list = M('stanfordmaterials_nav')->order('`order_num` desc,`id` desc')->select();
        $category_list = Category::toLevel($category_list, '&nbsp;&nbsp;&nbsp;&nbsp;',0);
        //dump($category_list);
        $this->assign('info', $category_list);
        $this->display();
    }

    /**
     * 添加站点
     */
    public function addNav()
    {
        $category_list = M('stanfordmaterials_nav')->where('status=1')->order('`order_num` desc,`id` desc')->select();
        $category_list = Category::toLevel($category_list, '&nbsp;&nbsp;&nbsp;&nbsp;',0);
        //dump($category_list);
        $this->assign('info', $category_list);
        $this->display();
    }



    /**
     * ajax添加导航
     */
    public function addNewNav()
    {
        $data = I('post.');
        if(empty($data['nav'])){
             returnAjaxJson(false,'请填写导航名称');
        }
        if(empty($data['order_num'])){
             returnAjaxJson(false,'请填写导航排序');
        }
        $parent_id = intval($data['parent_id']);
        $userinfo =  erpUserInfo();
        $userItem = [
            'nav'         => $data['nav'],
            'order_num'   => $data['order_num'],
            'parent_id'   => $parent_id,
            'create_time' => date('Y-m-d H:i:s',time()),
            'create_user' => $userinfo['username'],
            'update_user' => $userinfo['username'],
            'update_time' => date('Y-m-d H:i:s',time()),
        ];
        $id = M('stanfordmaterials_nav')->add($userItem);
        if($id){
            echo returnAjaxJson(true,'导航添加成功');
        }else{
            echo returnAjaxJson(false,'发生意料之外的错误');
        }
    }




    /**
     * 删除导航
     */
    public function delNav()
    {
        $id = I('post.id');
        if(!$id){
            returnAjaxJson(false,'发生了意料之外的错误，请联系管理员');
        }
        $result = M('stanfordmaterials_nav')->where("id = $id")->delete();
        if($result){
            M('stanfordmaterials_nav')->where("parent_id=$id")->delete();
            returnAjaxJson(true,'删除完成。');
        }else{
            returnAjaxJson(false,'发生意料之外的错误!!!');
        }
    }




    /**
     * 下架导航
     */
    public function cancelPutaway()
    {
        $id = I('post.id');
        $userinfo =  erpUserInfo();
        if(!$id){
            returnAjaxJson(false,'发生了意料之外的错误，请联系管理员');
        }
        $item = [
            'update_user'  => $userinfo['username'],
            'update_time'  => date('Y-m-d H:i:s',time()),
            'status'       => 2,
        ];
        $result = M('stanfordmaterials_nav')->where("id = $id")->save($item);
        M('stanfordmaterials_nav')->where("parent_id = $id")->save($item);
        if($result){
            returnAjaxJson(true,'下架导航成功');
        }else{
            returnAjaxJson(false,'发生意料之外的错误');
        }
    }

    /**
     * 上架导航
     */
    public function Putaway()
    {
        $id = I('post.id');
        $userinfo =  erpUserInfo();
        if(!$id){
            returnAjaxJson(false,'发生了意料之外的错误，请联系管理员');
        }
        $item = [
            'update_user'  => $userinfo['username'],
            'update_time'  => date('Y-m-d H:i:s',time()),
            'status'       => 1,
        ];
        $result = M('stanfordmaterials_nav')->where("id = $id")->save($item);
        M('stanfordmaterials_nav')->where("parent_id = $id")->save($item);
        if($result){
            returnAjaxJson(true,'上架导航成功');
        }else{
            returnAjaxJson(false,'发生意料之外的错误');
        }
    }


    /**
     * 展示修改导航详情
     */
    public function updateNavDetail()
    {
        $id = I('get.id');
        if(!$id){
            echo '这是一个美丽的错误，请联系管理员';
            die;
        }
        $info = M('stanfordmaterials_nav')->find($id);
        $category_list = M('stanfordmaterials_nav')->where('status=1')->order('`order_num` desc,`id` desc')->select();
        $category_list = Category::toLevel($category_list, '&nbsp;&nbsp;&nbsp;&nbsp;',0);
        $this->assign('list',$category_list);
        //dump($info);
        $this->assign('info',$info);
        $this->display();
    }



    /**
     * 更新导航信息
     */
    public function saveUpdateNavData()
    {
        $data = I('post.');
        if(empty($data['navId'])){
            returnAjaxJson(false,'未找到对应ID！');
        }
        $id = intval($data['navId']);
        if(empty($data['nav'])){
             returnAjaxJson(false,'请填写导航名称');
        }
        if(empty($data['order_num'])){
             returnAjaxJson(false,'请填写导航排序');
        }
        $parent_id = intval($data['parent_id']);
        $userinfo =  erpUserInfo();
        $userItem = [
            'nav'         => $data['nav'],
            'order_num'   => $data['order_num'],
            'parent_id'   => $parent_id,
            'update_user' => $userinfo['username'],
            'update_time' => date('Y-m-d H:i:s',time()),
        ];
        $id = M('stanfordmaterials_nav')->where("id=$id")->save($userItem);
        if($id){
             returnAjaxJson(true,'导航修改成功');
        }else{
             returnAjaxJson(false,'修改失败');
        }
    }











}