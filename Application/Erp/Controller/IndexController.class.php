<?php
/**
 * IndexController.class.php
 * User: Eric
 * Date: 2018/3/14
 * Time: 11:00
 * Project: OceaniaErp
 */
namespace Erp\Controller;
use Think\Controller;
class IndexController extends ErpController
{
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * erp首页
     */
    public function index()
    {
        $this->display();
    }
}