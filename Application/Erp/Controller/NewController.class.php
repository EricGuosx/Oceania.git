<?php
/**
 * NewController.class.php
 * User: Eric
 * Date: 2018/4/8
 * Time: 14:52
 * Project: OceaniaErp
 */
namespace Erp\Controller;
use Think\Controller;
use Common\Helper\Category;
class NewController extends ErpController
{
    public function __construct ()
    {
        parent::__construct();
    }

    /**
     * 添加新消息
     */
    public function addNews()
    {
        $webinfo = $_SESSION['website'];
        if(!$webinfo){
            $result = M('erp_website')->where('status=2')->find();
            $webinfo = $result['website'];
            $_SESSION['website'] = $result['website'];
        }
        $web = $webinfo;
        $this->assign('info',$web);
        $this->display();
    }

    /**
     * 保存news信息
     */
    public function saveNews()
    {
        $data = I('post.');
        if(empty($data['title'])){
            returnAjaxJson(false,'请填写文章title');
        }
        if(empty($data['website'])){
            returnAjaxJson(false,'请填写站点名称');
        }
        if(empty($data['seotitle'])){
            returnAjaxJson(false,'请填写SEO Title');
        }
        if(empty($data['seodescription'])){
            returnAjaxJson(false,'请填写SEO Description');
        }
        if(empty($data['static_html'])){
            returnAjaxJson(false,'界面链接不能为空');
        }
        $keywords1 = $data['seokeywords'];
        $count = count($keywords1);
        $keywords2 = twoToOne($keywords1);
        $keywords3 = implode(',',$keywords2);
        if(empty($keywords3) || $count<1){
            returnAjaxJson(false,'请填写SEO Keywords');
        }
        $userinfo =  erpUserInfo();
        $item = [
            'title' => $data['title'],
            'website' => $data['website'],
            'seodescription' => $data['seodescription'],
            'seotitle' => $data['seotitle'],
            'seokeywords' => $keywords3,
            'static_html' => $data['static_html'],
            'create_time' => date('Y-m-d H:i:s',time()),
            'create_user' => $userinfo['username'],
            'update_user' => $userinfo['username'],
            'update_time' => date('Y-m-d H:i:s',time()),
            'js_one'      => $data['js_one'],
            'js_two'      => $data['js_two'],
            'js_three'    => $data['js_three'],
        ];
        $id = M('stanfordmaterials_news')->add($item);
        if($id){
            returnAjaxJson(true,'news添加成功,请进后续详情添加');
        }else{
            returnAjaxJson(false,'系统异常');
        }
    }

    /**
     * 展示更新界面
     */
    public function updateNews()
    {
        $id = I('get.id');
        $id = intval($id);
        if(empty($id)){
            echo '非法操作，请联系管理员';die;
        }
        $data = M('stanfordmaterials_news')->find($id);
        //dump($data);
        $keyword = oneToTwo($data['seokeywords']);
        $keyword3 = json_encode($keyword);
        $this->assign('key',$keyword3);
        $this->assign('info',$data);
        $this->display();
    }

    /**
     * 保存更新信息
     */
    public function saveUpdateNews()
    {
        $data = I('post.');
        $id = intval($data['newsid']);
        if(empty($id)){
            returnAjaxJson(false,'缺少ID！！！');
        }
        if(empty($data['title'])){
            returnAjaxJson(false,'请填写文章title');
        }
        if(empty($data['website'])){
            returnAjaxJson(false,'请填写站点名称');
        }
        if(empty($data['seotitle'])){
            returnAjaxJson(false,'请填写SEO Title');
        }
        if(empty($data['seodescription'])){
            returnAjaxJson(false,'请填写SEO Description');
        }
        if(empty($data['static_html'])){
            returnAjaxJson(false,'界面链接不能为空');
        }
        $keywords1 = $data['seokeywords'];
        $count = count($keywords1);
        $keywords2 = twoToOne($keywords1);
        $keywords3 = implode(',',$keywords2);
        if(empty($keywords3) || $count<1){
            returnAjaxJson(false,'请填写SEO Keywords');
        }
        $userinfo =  erpUserInfo();
        $item = [
            'title' => $data['title'],
            'website' => $data['website'],
            'seodescription' => $data['seodescription'],
            'seotitle' => $data['seotitle'],
            'seokeywords' => $keywords3,
            'static_html' => $data['static_html'],
            'update_user' => $userinfo['username'],
            'update_time' => date('Y-m-d H:i:s',time()),
            'js_one'      => $data['js_one'],
            'js_two'      => $data['js_two'],
            'js_three'    => $data['js_three'],
        ];
        $id = M('stanfordmaterials_news')->where("id=$id")->save($item);
        if($id){
            returnAjaxJson(true,'news更新成功,请进后续详情添加');
        }else{
            returnAjaxJson(false,'系统异常');
        }
    }

    /**
     * 删除News
     */
    public function delNews()
    {
        $id = I('post.id');
        $id = intval($id);
        if(empty($id)){
            returnAjaxJson(false,'系统不稳定，请联系管理员');
        }
        $result = M('stanfordmaterials_news')->delete($id);
        //删除子类
        M('stanfordmaterials_news_sub')->where("news_id=$id")->delete();
        if($result){
            returnAjaxJson(true,'new删除成功');
        }else{
            returnAjaxJson(false,'系统异常');
        }
    }


    /**
     * 新闻列表
     */
    public function newLists()
    {
        $m = M('stanfordmaterials_news');
        $count = $m->count();
        $p = getpage($count,10);
        $list = M('stanfordmaterials_news')->field(true)->order('id DESC')->limit($p->firstRow, $p->listRows)->select();
        $this->assign('info', $list);
        $this->assign('page', $p->show());
        $this->display();
    }

    /**
     * 子类news
     */
    public function subNews()
    {
        $id = I('get.id');
        //echo $id;die;
        $this->assign('newsid',$id);
        $data = M('stanfordmaterials_news_sub')->where("news_id=$id")->select();
        //dump($data);
        $this->assign('data',$data);
        $this->display();
    }

    /**
     * 添加子类详情
     */
    public function addSubNews()
    {
        $id = I('get.newsid');
        $this->assign('newsid',$id);
        $this->display();
    }

    /**
     * 保存子类NEWS
     */
    public function saveSubNews()
    {
        $data = I('post.');
        $newsid= intval($data['newsid']);
        if(empty($newsid)){
            returnAjaxJson(false,'系统不稳定，请联系管理员');
        }
        if(empty($data['title'])){
            returnAjaxJson(false,'请填写标题');
        }
        if(empty($data['anchor'])){
            returnAjaxJson(false,'请填写锚点名称');
        }
        if(empty($data['content'])){
            returnAjaxJson(false,'请填写内容');
        }
        if(empty($data['order_status'])){
            returnAjaxJson(false,'排序字段不能为空');
        }
        $userinfo =  erpUserInfo();
        $item = [
            'title'   => $data['title'],
            'anchor'  => $data['anchor'],
            'content' => $data['content'],
            'news_id' => $newsid,
            'order_status' => $data['order_status'],
            'create_time' => date('Y-m-d H:i:s',time()),
            'create_user' => $userinfo['username'],
            'update_user' => $userinfo['username'],
            'update_time' => date('Y-m-d H:i:s',time()),
        ];
        $result = M('stanfordmaterials_news_sub')->add($item);
        if($result){
            returnAjaxJson(true,'子类添加成功');
        }else{
            returnAjaxJson(false,'系统不稳定，请联系管理员');
        }
    }

    /**
     * 展示子类信息
     */
    public function showUpdateNews()
    {
        $id = I('get.id');
        $id = intval($id);
        $data = M('stanfordmaterials_news_sub')->find($id);
        $this->assign('info',$data);
        //dump($data);
        $this->display();
    }

    /**
     * 保存更新的子类信息
     */
    public function saveUpdateSubNews()
    {
        $data = I('post.');
        $id = intval($data['subnewsid']);
        if(empty($id)){
            returnAjaxJson(false,'缺少ID，系统异常');
        }
        if(empty($data['title'])){
            returnAjaxJson(false,'请填写标题');
        }
        if(empty($data['anchor'])){
            returnAjaxJson(false,'请填写锚点名称');
        }
        if(empty($data['content'])){
            returnAjaxJson(false,'请填写内容');
        }
        if(empty($data['order_status'])){
            returnAjaxJson(false,'排序字段不能为空');
        }
        $userinfo =  erpUserInfo();
        $item = [
            'title'   => $data['title'],
            'anchor'  => $data['anchor'],
            'content' => $data['content'],
            'order_status' => $data['order_status'],
            'update_user' => $userinfo['username'],
            'update_time' => date('Y-m-d H:i:s',time()),
        ];
        $result = M('stanfordmaterials_news_sub')->where("id=$id")->save($item);
        if($result){
            returnAjaxJson(true,'子类更新成功');
        }else{
            returnAjaxJson(false,'系统不稳定，请联系管理员');
        }
    }

    /**
     * 删除子类信息
     */
    public function delSubNews()
    {
        $id = I('post.id');
        $id = intval($id);
        if(empty($id)){
            returnAjaxJson(false,'系统异常，请联系管理员!!!');
        }
        $result = M('stanfordmaterials_news_sub')->delete($id);
        if($result){
            returnAjaxJson(true,'删除完成');
        }else{
            returnAjaxJson(false,'系统发生错，请联系管理员');
        }
    }





    
    
    
    
    
    
}