<?php
/**
 * ErpController.class.php
 * User: Eric
 * Date: 2018/3/14
 * Time: 11:30
 * Project: OceaniaErp
 */
namespace Erp\Controller;
use Think\Controller;
class ErpController extends Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->checkLogin();
    }

    /**
     * 登录验证
     *
     * @access public
     */
    public function checkLogin()
    {
        $info = $_SESSION;
        if(empty($info))
        {
            header("Content-Type:text/html;charset=utf-8");
            $prompt = "<html xmlns=\"http://www.w3.org/1999/xhtml\">
 <head>
     <title>Oceania-UBMC</title>
     <link href=\"/assets/favicon-950947d692935bf7e0b1629a69cd89ed.ico\" rel=\"shortcut icon\" type=\"image/vnd.microsoft.icon\">
     <meta http-equiv=\"Content-Type\" content=\"text/html; charset=UTF-8\"/>
     <style>
         body {margin-top:100px;background:#fff;font-family: Verdana, Tahoma;}
         a {color:#CE4614;}
         #msg-box {color: #CE4614; font-size:0.9em;text-align:center;}
         #msg-box .logo {border-bottom:5px solid #ECE5D9;margin-bottom:20px;padding-bottom:10px;}
         #msg-box .title {font-size:1.4em;font-weight:bold;margin:0 0 30px 0;}
         #msg-box .nav {margin-top:20px;}
     </style>
 </head>
 <body>
 <div id='msg-box'>
     <div class='logo'><a href=\"/\"><img src=\"Public/Erp/Admin/images/logo.png\" border=\"0\"></a></div>
     <div class='title'>欢迎浏览我们的网站，您可能没有登录，请稍等片刻。</div>

     <div class='msg'>正在跳转登录中...</div>
 </div>
 </body>
 </html>";
            $this->redirect('Admin/login', array(),1, $prompt);
        }
    }
}