<?php
/**
 * AdminController.class.php
 * User: Eric
 * Date: 2018/3/14
 * Time: 13:22
 * Project: OceaniaErp
 */
namespace Erp\Controller;
use Think\Controller;
class AdminController extends Controller
{
    /**
     *  管理员登录
     *
     * @access public
     */
    public function login()
    {
        if($_POST['username']){
            $content = I('post.');
            $map     = [
                'nickname' => $content['username'],
                'password' => md5($content['password'] . 'Oceania'),
            ];
            $info = M('erp_admin')->where($map)->find();
            if($info){
                $online_time = date('Y-m-d H:i:s',time());
                $map = [
                    'online_time' => $online_time,
                ];
                $_SESSION = [
                    'userid'       => $info['id'],
                    'username'     => $info['username'],
                    'online_time' => $online_time,
                ];
                $id = $info['id'];
                M('erp_admin')->where("id = $id")->save($map);
                $log_item = [
                    'create_user' => $info['username'],
                    'content'     => '登陆UBMC系统',
                    'ip'          => get_ip()
                ];
                M('erp_login_log')->add($log_item);
                //$this->success('登陆成功',U('Index/index'),1);
                $prompt = "<html xmlns=\"http://www.w3.org/1999/xhtml\">
 <head>
     <title>Oceania-UBMC</title>
     <link href=\"/assets/favicon-950947d692935bf7e0b1629a69cd89ed.ico\" rel=\"shortcut icon\" type=\"image/vnd.microsoft.icon\">
     <meta http-equiv=\"Content-Type\" content=\"text/html; charset=UTF-8\"/>
     <style>
         body {margin-top:100px;background:#fff;font-family: Verdana, Tahoma;}
         a {color:#CE4614;}
         #msg-box {color: #CE4614; font-size:0.9em;text-align:center;}
         #msg-box .logo {border-bottom:5px solid #ECE5D9;margin-bottom:20px;padding-bottom:10px;}
         #msg-box .title {font-size:1.4em;font-weight:bold;margin:0 0 30px 0;}
         #msg-box .nav {margin-top:20px;}
     </style>
 </head>
 <body>
 <div id='msg-box'>
     <div class='logo'><a><img src=\"/Public/Erp/Admin/images/logo.png\" border=\"0\"></a></div>
     <div class='title'>欢迎浏览我们的网站，如有任何疑问请联系管理员Eric.Guo@oceania-inc.com。</div>

     <div class='msg'>正在跳转登录中...</div>
 </div>
 </body>
 </html>";
                $this->redirect('Index/index', [],1, $prompt);
            }else{
                //$this->error('账号密码错误','',1);
                $prompt = "<html xmlns=\"http://www.w3.org/1999/xhtml\">
 <head>
     <title>Oceania-UBMC</title>
     <link href=\"/assets/favicon-950947d692935bf7e0b1629a69cd89ed.ico\" rel=\"shortcut icon\" type=\"image/vnd.microsoft.icon\">
     <meta http-equiv=\"Content-Type\" content=\"text/html; charset=UTF-8\"/>
     <style>
         body {margin-top:100px;background:#fff;font-family: Verdana, Tahoma;}
         a {color:#CE4614;}
         #msg-box {color: #CE4614; font-size:0.9em;text-align:center;}
         #msg-box .logo {border-bottom:5px solid #ECE5D9;margin-bottom:20px;padding-bottom:10px;}
         #msg-box .title {font-size:1.4em;font-weight:bold;margin:0 0 30px 0;}
         #msg-box .nav {margin-top:20px;}
     </style>
 </head>
 <body>
 <div id='msg-box'>
     <div class='logo'><a href=\"/\"><img src=\"/Public/Erp/Admin/images/logo.png\" border=\"0\"></a></div>
     <div class='title'>账号密码错误。</div>

     <div class='msg'>正在跳转登录中...</div>
 </div>
 </body>
 </html>";
                $this->redirect('Admin/login', array(),1, $prompt);
            }
        }else{
            $this->display();
        }
    }


    /**
     *  退出登录
     *
     * @access public
     */
    public function logout()
    {
        $_SESSION =[];
        session_destroy();
        header('location:./login');
    }
}